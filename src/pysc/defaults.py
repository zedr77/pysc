PROTOCOL = "https"
API_DOMAIN = "socialcast.com"
API_ROOT = "api"
API_NETWORK = "demo"
DEMO_CREDS = {
    'email': 'emily@socialcast.com',
    'password': 'demo'
}
SAFE_CHARS = '[]&='
