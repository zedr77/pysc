"""
Interfaces for the implementation of Socialcast resources.

"""
from abc import ABCMeta, abstractproperty


class AbstractResource:
    """An interface to an HTTP resource of the Socialcast API.
    """
    __metaclass__ = ABCMeta

    @abstractproperty
    def location(self):
        pass

    @classmethod
    def __subclasshook__(cls, C):
        for klass in C.mro():
            if klass is AbstractResource:
                return True
        return NotImplemented
