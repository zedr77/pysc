"""
Socialcast Portal objects.

"""
from pysc.api import JsonApi as Api
from pysc.defaults import API_NETWORK
from pysc.exceptions import NotLoggedInError


class Socialcast(object):

    def __init__(self, *args):
        network_name = args[0] if args else API_NETWORK
        self.communities = None
        self.api = Api(network_name)

    def login(self, email, password):
        response = self.api.authentication.login(email=email,
                                                 password=password)
        if response.ok:
            data = self.api.parse_response(response)
            self.communities = data['communities']

    @property
    def user(self):
        # the ident is always the first member of the credentials tuple
        try:
            return self.api.credentials[0]
        except (AttributeError, KeyError, IndexError, TypeError):
            pass
