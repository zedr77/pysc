"""
Socialcast API Resource plugin loaders

"""
import os
import re
from importlib import import_module


class ResourceLoader(object):
    namespace = __name__.split('.')[0]
    dir_name = 'resources'

    def __init__(self, path=None):
        file_path = os.path.abspath(__file__) if path is None else path
        base_path = os.path.split(file_path)[0]
        self.resource_path = os.path.join(base_path, self.dir_name)

    @property
    def names(self):
        regexp = re.compile('([a-z]+)\.py$')
        names = os.listdir(self.resource_path)
        return [name.rstrip('.py') for name in names if regexp.match(name)]

    def load(self, plugin_name):
        dotted_name = ".".join((self.namespace, self.dir_name, plugin_name))
        module = import_module(dotted_name)
        resource_cls = getattr(module, plugin_name.capitalize())
        return resource_cls if hasattr(resource_cls, 'relative_path') else None

    def load_all(self):
        names = self.names
        return [obj for obj in (self.load(name) for name in names) if obj]
