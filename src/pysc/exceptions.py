class GenericApiError(Exception):
    """The base exception for pysc.
    """

class NotLoggedInError(GenericApiError):
    """The request failed because the user client has not authenticated.
    """
