"""
Handy dictionary subclasses.

"""
from urllib import quote

from pysc.defaults import SAFE_CHARS


class ParamDict(dict):
    def __init__(self, name, *args, **kwargs):
        self.name = name
        self.update(kwargs)

    def __str__(self):
        seq = ["=".join((k, str(v))) for k, v in self.items() if v is not None]
        return quote("&".join(seq), safe=SAFE_CHARS)

    def __setitem__(self, item, value):
        new_item = "%s[%s]" % (self.name, item)
        return super(ParamDict, self).__setitem__(new_item, value)

    def update(self, *args, **kwargs):
        for k, v in dict(*args, **kwargs).iteritems():
            self[k] = v

    def __repr__(self):
        return "%s('%s', %s)" % (self.__class__.__name__, self.name,
                                 dict(self))


class QueryDict(dict):
    def __str__(self):
        pairs = self.items()
        seq = ["=".join((k, str(v))) for k, v in pairs if v is not None]
        return quote("?" + "&".join(seq), safe=SAFE_CHARS + '?')

    def startswith(self, substr):
        return self.__str__()[0:len(substr)] == substr

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, dict(self))
