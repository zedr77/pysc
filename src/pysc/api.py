"""
Socialcast API objects.
"""
import weakref

import requests
from requests.auth import HTTPBasicAuth

from pysc.loaders import ResourceLoader
from pysc import defaults


class PluginBasedApiMeta(type):
    def __new__(typ, name, bases, namespace):
        loader = ResourceLoader()
        ns = namespace.copy()
        ns.update({
            '_resource_plugins': loader.load_all()
        })
        return type(name, bases, ns)


class BaseApi(object):
    """A basic HTTP based API.
    """
    __metaclass__ = PluginBasedApiMeta

    _resource_plugins = None

    media_type = ''

    def __init__(self, network, secure=True, alt_domain=None, alt_root=None):
        self.protocol = "https" if secure else "http"
        self.network = network
        self.domain = alt_domain or defaults.API_DOMAIN
        self.root = alt_root or defaults.API_ROOT
        self.session = requests.Session()
        self.session.headers = {'accept': getattr(self, 'media_type', '*/*')}
        self.credentials = None
        self.content_type = self.media_type.split('/')[-1]

        for resource_cls in self._resource_plugins:
            if resource_cls._is_top_level:
                self.register_resource(resource_cls())

    @property
    def host(self):
        return "{PROTOCOL}://{NETWORK}.{DOMAIN}".format(**{
            'PROTOCOL': self.protocol,
            'NETWORK': self.network,
            'DOMAIN': self.domain,
        })

    @property
    def absolute_path(self):
        return self.host

    @property
    def plain_auth(self):
        creds = self.credentials
        return HTTPBasicAuth(*creds) if creds else None

    def register_resource(self, resource):
        setattr(self, resource.name, resource)
        resource._api = weakref.proxy(self)

    def parse_response(self, response):
        accept = self.content_type
        try:
            parser = getattr(response, accept)
        except AttributeError:
            raise AttributeError('Parser for %s not implemented.' % accept)

        return parser()


class JsonApi(BaseApi):
    media_type = "application/json"
