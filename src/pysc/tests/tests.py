import re
import unittest
import random

from pysc.abstract import AbstractResource
from pysc.loaders import ResourceLoader
from pysc.resources import BaseResource
from pysc.defaults import DEMO_CREDS
from pysc.api import BaseApi
from pysc import Socialcast


class DummyResource(BaseResource):
    relative_path = "/api/dummy"


class AbstractTests(unittest.TestCase):

    def test__simple_resource(self):
        """It implements an abstract class correctly.
        """
        self.assertTrue(issubclass(DummyResource, AbstractResource))

    def test__resource_loader(self):
        """It loads and sets resource endpoints on the api object.
        """
        portal = Socialcast()
        self.assertTrue(hasattr(portal, 'api'))


class LoaderTests(unittest.TestCase):
    def setUp(self):
        self.loader = ResourceLoader()

    def test__loader_has_plugins(self):
        matcher = re.compile('^[a-z]+$')
        names = self.loader.names

        self.assertGreater(names, 0)

        for name in names:
            self.assertIsNot(matcher.match(name), None)

    def test__loaded_classes_implement_abstract(self):
        loader = self.loader

        for name in loader.names:
            cls = loader.load(name)
            if cls:
                self.assertTrue(issubclass(cls, AbstractResource))
            else:
                self.assertIs(cls, None)


class PortalTests(unittest.TestCase):

    def setUp(self):
        self.portal = Socialcast()

    def tearDown(self):
        del(self.portal)

    def test__portal_has_api(self):
        self.assertTrue(hasattr(self.portal, 'api'))

    def test__portal_can_login_and_has_communities(self):
        self.portal.login(**DEMO_CREDS)
        self.assertTrue(self.portal.communities)


class ApiTestCase(unittest.TestCase):
    def setUp(self):
        self.portal = Socialcast()
        self.portal.login(**DEMO_CREDS)

    def tearDown(self):
        del(self.portal)


class ApiTests(ApiTestCase):

    def test__api_has_correct_base_location(self):
        """The instantiated API object has an attribute named "host",
        which is a correct HTTP url, according to the default and passed
        parameters.
        """
        api = BaseApi('demo')
        expected = 'https://demo.socialcast.com'
        self.assertEquals(api.host, expected)

    def test__resource_loader(self):
        """It loads and sets resource endpoints on the api object.

        For resource module, test that its name is set as an
        attribute on the api object of the portal, as is seen as
        deriving from the abstract class of resources.
        """
        api = self.portal.api
        loader = ResourceLoader()

        # At least one plugin
        self.assertGreater(len(loader.names), 0)

        for name in loader.names:
            if loader.load(name) is not None:
                resource = getattr(api, name)
                self.assertTrue(hasattr(resource, '_session'))
                self.assertTrue(hasattr(resource, '_host'))

    def test__resources_have_wrapper_methods(self):
        """It has method wrappers that curry the session object methods that
        implement http requests with different verbs.

        Wrapper methods are invoked by prefixing them with "http_".
        """
        verbs = ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS', 'PATCH']
        for verb in verbs:
            # FIXME: do for all resources
            api = self.portal.api
            obj = getattr(api.authentication, verb)
            self.assertTrue(isinstance(obj, object))

    def test__live_authentication(self):
        portal = self.portal
        # Successful login
        self.assertEqual(DEMO_CREDS['email'], self.portal.user)
        # Communities are loaded, and at least one community.
        self.assertGreater(len(portal.communities), 0)


class MessagesTests(ApiTestCase):
    def test__live_messages(self):
        portal = self.portal
        self.assertTrue(hasattr(portal.api, 'credentials'))
        self.assertTrue(hasattr(portal.api.messages, '__iter__'))

    def test__live_message_count(self):
        self.assertTrue(len(self.portal.api.messages), 0)

    def test__live_message_get(self):
        """Get a random message.
        """
        message_idxs = list(self.portal.api.messages)
        idx = random.choice(message_idxs)
        msg = self.portal.api.messages[idx]
        self.assertEqual(msg['id'], idx)

    def test__live_messages_iterator(self):
        # Check only three messages.
        messages = self.portal.api.messages
        for idx in messages:
            msg = messages[idx]
            self.assertEquals(msg['id'], idx)
            break

    def test__live_messages_search(self):
        """A search for the letter 'e' should yield at least one result.
        """
        messages = self.portal.api.messages.search('e')
        self.assertGreater(len(messages), 0)
        for idx in messages:
            self.assertEquals(idx, messages[idx]['id'])

    def test__live_message_delete(self):
        idx = random.choice(list(self.portal.api.messages))
        del self.portal.api.messages[idx]
        self.assertIn(idx, list(self.portal.api.messages))


class PlainAuthTests(unittest.TestCase):
    def test__login(self):
        pass


if __name__ == '__main__':
    unittest.main()
