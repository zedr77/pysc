"""
Socialcast 'Messages' resource.

"""

from pysc.resources import CollectionResource
from pysc.resources.message import Message


class Messages(CollectionResource):
    relative_path = '/api/messages'
    item_resource = Message

    def _new(self, title, **kwargs):
        msg = {'title': title}
        msg.update(kwargs)
        return self._create(msg)

    def __delitem__(self, key):
        try:
            self[key]._delete()
        except KeyError:
            pass    # FIXME

