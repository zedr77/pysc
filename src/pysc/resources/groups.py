"""
Socialcast 'Messages' resource.

"""
from pysc.resources import CollectionResource


class Groups(CollectionResource):
    relative_path = '/api/groups'
