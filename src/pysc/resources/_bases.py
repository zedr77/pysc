"""
Socialcast base resource class.

"""
import logging
import re

from pysc.abstract import AbstractResource
from pysc.exceptions import GenericApiError, NotLoggedInError
from pysc.dicts import ParamDict, QueryDict


class BaseResource(object):
    """A base API resource.
    """
    _http_rxp = re.compile("^GET|POST|PUT|DELETE|OPTIONS|PATCH|HEAD$")

    @property
    def _is_top_level(self):
        """Check if this resource was installed on the API as a top level
        resource.
        """
        root = getattr(self, 'root')
        match = re.match('^/' + root + '/[a-zA-Z]+$', self.location)
        return True if match else False

    @property
    def name(self):
        """The canonical name for this resource.
        """
        return getattr(self, 'alt_name', self.__class__.__name__.lower())

    @property
    def absolute_path(self):
        return self._host + getattr(self, 'relative_path', '<undefined>')

    @property
    def _host(self):
        return self._api.host

    @property
    def _session(self):
        return self._api.session

    def _parse_response(self, response):
        return self._api.parse

    def __getattr__(self, item):
        if self._http_rxp.match(item):
            def method_wrapper(*args, **kw):
                path = self.absolute_path
                method = getattr(self._session, item.lower())
                if args:
                    obj = args[0]
                    if hasattr(obj, "startswith"):
                        path = "%s/%s" % (path, obj)
                    else:
                        kw['params'] = obj
                kw['auth'] = self._api.plain_auth

                response = method(path, **kw)

                try:
                    response.data = self._api.parse_response(response)
                except ValueError:
                    response.data = None

                if response.status_code == 401:
                    self._log('error', response)
                    raise NotLoggedInError(
                        "Authentication failed: %s" % response.reason)

                return response

            return method_wrapper

        raise AttributeError("'%s'" % item)

    def __repr__(self):
        return "%s()" % self.__class__.__name__

    def _log(self, level, response):
        lvl = getattr(logging, level.upper())
        msg = "%s got [%d %s] @ %s" % (response.request.method,
                                       response.status_code,
                                       response.reason,
                                       self.absolute_path)
        logging.log(lvl, msg)


AbstractResource.register(BaseResource)


class CollectionResource(BaseResource):
    """A generic collection resource.
    """
    item_resource = None

    def keys(self):
        return sorted(list(self))

    def search(self, q, **kwargs):
        query = QueryDict()
        query['q'] = q
        return self._fetch(**query)

    def _fetch(self, **kwargs):
        seq = self.GET(kwargs).data[self.name]
        # FIXME: find a standard way to get the name of the item
        # resource
        name = self.item_resource.__name__.lower()
        return dict((el['id'], self.item_resource({name: el})) for el in seq)

    def _create(self, namespace):
        item_resource_name = self.item_resource.__name__.lower()
        response = self.POST(ParamDict(item_resource_name, **namespace))
        return response.reason

    def _read(self, ident):
        response = self.GET("%d" % ident)
        if response.ok:
            return self.item_resource(response.data, api=self._api)

    def __getitem__(self, item):
        return self._read(item)

    def __setitem__(self, item, value):
        raise NotImplementedError

    def __iter__(self):
        return iter(self._fetch())

    def __len__(self):
        return len(self._fetch())


class ItemResource(BaseResource, dict):
    def __init__(self, data, api=None):
        super(ItemResource, self).__init__()
        self.update(data[self.name])
        self._api = api

    @property
    def absolute_path(self):
        return self.get('url', None)

    @property
    def id(self):
        return self['id']

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, dict(self))

