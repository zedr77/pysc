"""
SocialCast `Authentication` resource.

"""
from pysc.resources import BaseResource


class Authentication(BaseResource):
    relative_path = "/api/authentication"

    def login(self, email=None, password=None, auth=None):
        if email and password:
            response = self.POST({'email': email, 'password': password})
            if response.ok:
                self._api.credentials = (email, password)

            return response
