"""
Socialcast message item resource.

"""
from pysc.resources import ItemResource


class Message(ItemResource):

    def _delete(self):
        self.DELETE()
