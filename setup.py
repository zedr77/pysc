from setuptools import setup, find_packages


version = '0.3'

long_description = open('README.txt').read()+ '\n'

setup(name='pysc',
      version='0.2.0',
      description='A Python wrapper for the Socialcast API',
      author='Rigel Di Scala',
      author_email='zedr@zedr.com',
      url='http://github.com/zedr/socialcast',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      include_package_data=True,
      install_requires=['requests'])
